script_name("Notepad")
script_description("/notepad || /note || /folders")
script_author("Wolter")
script_dependencies("SAMPFUNCS & SAMP")
script_version("v1.1.3")
editingActivated = false
local LIP = {};

function main()
  if not isCleoLoaded() or not isSampfuncsLoaded() or not isSampLoaded() then return end
  while not isSampAvailable() do wait(100) end
  if not doesDirectoryExist("moonloader\\config") then createDirectory("moonloader\\config") end
  if not doesFileExist("moonloader\\config\\Notepad.ini") then
    local data =
    {
        folders = {},
        text = {},
    };
    LIP.save('moonloader\\config\\Notepad.ini', data); --
  end
  sampRegisterChatCommand("notepad", cmdOpenNote)
  sampRegisterChatCommand("note", cmdNewNote)
  sampRegisterChatCommand("folder", cmdNewFolder)
  while true do
    wait(0)
    if editingActivated then
      dia = sampShowDialog(473, "{f8f32b}�������", noteText, "Edit", "Close", 2)
      sampSetCurrentDialogListItem(0)
      repeat
        wait(0)
        re, button, list, input = sampHasDialogRespond(473)
      until re
      if button == 0 then
        _ = sampCloseCurrentDialogWithButton()
      else
        noteString = sampGetListboxItemText(list)
        demoList = (list + 1) - numberFile
        if noteString == "{BBBBBB}Add a string..." then
          dia = sampShowDialog(879, string.format("{f8f32b}���������� ����� ������", demoList), string.format("{BBBBBB}������� ����� ������ � �������:", noteString), "OK", "Close", 1)
          repeat
            wait(0)
            re, button, list, input = sampHasDialogRespond(879)
          until re
          if button == 0 then
            _ = sampCloseCurrentDialogWithButton()
          else cmdNewNote(input) end
        else
          if demoList <= 0 then
            folderName = string.match(noteString, "{FF88FF}%[F%]{FFFFFF} (.*)")
            noteText = ""
            for i = 1, #data[folderName] do
              noteText = string.format("%s%s\n", noteText, data[folderName][i])
            end
            noteText = string.format("%s{BBBBBB}Add a string...", noteText)
            dia = sampShowDialog(472, string.format("{f8f32b}%s", folderName), noteText, "Edit", "Close", 2)
            sampSetCurrentDialogListItem(0)
            repeat
              wait(0)
              re, button, list, input = sampHasDialogRespond(472)
            until re
            if button == 0 then
              _ = sampCloseCurrentDialogWithButton()
            else
              noteString = sampGetListboxItemText(list)
              demoList = (list + 1)
              if noteString == "{BBBBBB}Add a string..." then
                dia = sampShowDialog(878, string.format("{f8f32b}���������� ����� ������", demoList), string.format("{BBBBBB}������� ����� ������ � �������,\n��� ������� \"delete\"(��� �������),\n����� ������� �����", noteString), "OK", "Close", 1)
                repeat
                  wait(0)
                  re, button, list, input = sampHasDialogRespond(878)
                until re
                if button == 0 then
                  _ = sampCloseCurrentDialogWithButton()
                else
                  data = LIP.load('moonloader\\config\\Notepad.ini');
                  if input == "delete" then
                    data[folderName] = nil
                    for i = 1, #data.folders do
                      if data.folders[i] == folderName then data.folders[i] = nil end
                    end
                    data.folders[folderName] = nil
                  else
                    table.insert (data[folderName], input)
                  end
                  LIP.save('moonloader\\config\\Notepad.ini', data);
                end
              else
                dia = sampShowDialog(877, string.format("{f8f32b}������ ����� %d", demoList), string.format("{BBBBBB}�������������� ������:\n{FFFFFF}%s\n{BBBBBB}������� ����� ���������� ������,\n��� �������� ���� ������, ����� ������� ������", noteString), "OK", "Close", 1)
                sampSetCurrentDialogEditboxText(data[folderName][demoList])
                repeat
                  wait(0)
                  re, button, list, input = sampHasDialogRespond(877)
                until re
                if button == 0 then
                  _ = sampCloseCurrentDialogWithButton()
                else
                  data = LIP.load('moonloader\\config\\Notepad.ini');
                  if input ~= "" then data[folderName][demoList] = input else
                    for i = demoList, #data[folderName] do
                      newI = i + 1
                      data[folderName][i] = data[folderName][newI]
                    end
                  end
                  LIP.save('moonloader\\config\\Notepad.ini', data);
                end
              end
            end
          else
            dia = sampShowDialog(880, string.format("{f8f32b}������ ����� %d", demoList), string.format("{BBBBBB}�������������� ������:\n{FFFFFF}%s\n{BBBBBB}������� ����� ���������� ������,\n��� �������� ���� ������, ����� ������� ������", noteString), "OK", "Close", 1)
            sampSetCurrentDialogEditboxText(data.text[demoList])
            repeat
              wait(0)
              re, button, list, input = sampHasDialogRespond(880)
            until re
            if button == 0 then
              _ = sampCloseCurrentDialogWithButton()
            else
              data = LIP.load('moonloader\\config\\Notepad.ini');
              if input ~= "" then data.text[demoList] = input else
                for i = demoList, #data.text do
                  newI = i + 1
                  data.text[i] = data.text[newI]
                end
              end
              LIP.save('moonloader\\config\\Notepad.ini', data);
            end
          end
        end
      end
      editingActivated = false
    end
  end
end

function cmdOpenNote()
  data = LIP.load('moonloader\\config\\Notepad.ini');
  noteString = ""
  noteFile = ""
  numberFile = 0
  for i = 1, #data.folders do
    numberFile = numberFile + 1
    noteFile = string.format("%s{FF88FF}[F]{FFFFFF} %s\n", noteFile, data.folders[i])
  end
  for i = 1, #data.text do
    noteString = string.format("%s%s\n", noteString, data.text[i])
  end
  noteText = string.format("%s%s{BBBBBB}Add a string...", noteFile, noteString)
  editingActivated = true
end

function cmdNewNote(newNoteText)
  data = LIP.load('moonloader\\config\\Notepad.ini');
  table.insert (data.text, newNoteText)
  LIP.save('moonloader\\config\\Notepad.ini', data);
end

function cmdNewFolder(newNoteFolder)
  data = LIP.load('moonloader\\config\\Notepad.ini');
  fCreate = true
  for i = 1, #data.folders do
    if newNoteFolder == data.folders[i] then fCreate = false end
  end
  if fCreate and newNoteFolder ~= "text" and newNoteFolder ~= "folders" then
    data[newNoteFolder] = {}
    table.insert (data.folders, newNoteFolder)
  end
  LIP.save('moonloader\\config\\Notepad.ini', data);
end

function LIP.load(fileName)
	assert(type(fileName) == 'string', 'Parameter "fileName" must be a string.');
	local file = assert(io.open(fileName, 'r'), 'Error loading file : ' .. fileName);
	local data = {};
	local section;
	for line in file:lines() do
		local tempSection = line:match('^%[([^%[%]]+)%]$');
		if(tempSection)then
			section = tonumber(tempSection) and tonumber(tempSection) or tempSection;
			data[section] = data[section] or {};
		end
		local param, value = line:match('^([%w|_]+)%s-=%s-(.+)$');
		if(param and value ~= nil)then
			if(tonumber(value))then
				value = tonumber(value);
			elseif(value == 'true')then
				value = true;
			elseif(value == 'false')then
				value = false;
			end
			if(tonumber(param))then
				param = tonumber(param);
			end
			data[section][param] = value;
		end
	end
	file:close();
	return data;
end

function LIP.save(fileName, data)
	assert(type(fileName) == 'string', 'Parameter "fileName" must be a string.');
	assert(type(data) == 'table', 'Parameter "data" must be a table.');
	local file = assert(io.open(fileName, 'w+b'), 'Error loading file :' .. fileName);
	local contents = '';
	for section, param in pairs(data) do
		contents = contents .. ('[%s]\n'):format(section);
		for key, value in pairs(param) do
			contents = contents .. ('%s=%s\n'):format(key, tostring(value));
		end
		contents = contents .. '\n';
	end
	file:write(contents);
	file:close();
end

return LIP;
